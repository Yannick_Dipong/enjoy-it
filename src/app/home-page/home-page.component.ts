import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  constructor(private _router:Router) { }

  loginButtonClick(): void{
    this._router.navigate(['/login'])
  };

  registerButtonClick(): void{
    this._router.navigate(['/register'])
  };

  ngOnInit() {
  }

}
