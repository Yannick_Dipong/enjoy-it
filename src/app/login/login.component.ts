import { Component, OnInit } from '@angular/core';
import { ValidateCredentialsService } from '../Services/validate-credentials.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  nameTest = "Sturidge";

  
  constructor(private validateCredentials: ValidateCredentialsService) {}  

  ngOnInit() {
  }

  displayData(){
    this.validateCredentials.validateUserName(this.nameTest);
    // console.log(this.nameTest);
  }

}
