import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})


export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  // Creation d'un objet de type booléan auquel on affecte la valeur "false"
  _opened: boolean = false;
  // Fonction exécutée lors du clic sur le bouton
  _toggleSidebar() {
    // Affectation d'une valeur différente a l'objet "_opened"
    console.log("this is a test: ", document.getElementById('img-text'));
    this._opened = !this._opened;

    if(window.screen.width < 992){

      document.getElementById('dashboard').style.marginLeft = "0";
      
    }
    else{
      if(this._opened == true){
        document.getElementById('dashboard').style.marginLeft = "180px";
      }else{
        document.getElementById('dashboard').style.marginLeft = "0";
      };
    }
}

   closeSideBar(){
    this._opened = false;
    document.getElementById('dashboard').style.marginLeft = "0";
  }
}

