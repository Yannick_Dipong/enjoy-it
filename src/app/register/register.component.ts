import { Component, OnInit } from '@angular/core';
import { ValidateCredentialsService} from "../Services/validate-credentials.service";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  nameTest = "";
  emailTest = "";
  passwordTest = "";
  confirmPassword = this.passwordTest;

  constructor(private validateCredentials: ValidateCredentialsService) { }


  ngOnInit() {
  }

  displayData(){
    this.validateCredentials.validateUserName(this.nameTest);
    this.validateCredentials.validateEmail(this.emailTest);
    this.validateCredentials.validatePassword(this.passwordTest);
    this.validateCredentials.confirmPassword(this.confirmPassword,this.passwordTest);
   
  }
  

}
