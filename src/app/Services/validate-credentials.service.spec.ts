import { TestBed } from '@angular/core/testing';

import { ValidateCredentialsService } from './validate-credentials.service';

describe('ValidateCredentialsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ValidateCredentialsService = TestBed.get(ValidateCredentialsService);
    expect(service).toBeTruthy();
  });
});
